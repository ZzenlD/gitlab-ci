# gitlab-ci

Collection of ready-made pipelines for easy integration into own repositories.

## Pipelines

Every application-specific pipeline automatically includes the basic.

| Name    | Description                                                     |
| :------ | :-------------------------------------------------------------- |
| basic   | Contains various lint-jobs, syntax checker, best-practice tools |
| ansible | Contains Ansible specific steps                                 |
| docker  | Contains docker specific steps                                  |
| hugo    | Contains hugo specific steps                                    |
| packer  | Contains packer specific steps                                  |
| rpm     | Contains rpm specific steps                                     |

## Integration

To avoid errors, the stages `build`, `test`, `deploy` must exist.

Copy the following code block to the _.gitlab-ci.yml_:

```yaml
include:
  - remote: "https://gitlab.com/ZzenlD/gitlab-ci/raw/master/<pipeline>.gitlab-ci.yml"
```

## Configuration

The pipelines can be influenced by variables.

### docker

| Name              | Example   | Default           | Description |
| :---------------- | :-------- | :---------------- | :---------- |
| registry_user     | foo       | <your repo-user>  |             |
| registry_password | abc-def   | <your repo-token> |             |
| registry_host     | docker.io | <your repo-host>  |             |

### hugo

| Name             | Example | Default | Description |
| :--------------- | :------ | :------ | :---------- |
| netlify_site_id  | abcdef  |         |             |
| netlify_password | abc-def |         |             |
